import express from 'express';

import {WelcomeController} from './controllers';

const app: express.Application = express();

app.set("port", process.env.PORT || 3000);

app.use('/welcome', WelcomeController);

const server = app.listen(app.get("port"), () => {
    console.log("Listening at http://localhost:%d/", app.get("port"));
});

export default server;
